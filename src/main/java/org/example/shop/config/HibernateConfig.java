package org.example.shop.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.example.shop.model.Cart;
import org.example.shop.model.Book;
import org.example.shop.model.Role;
import org.example.shop.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan(value = "org.example.shop")
public class HibernateConfig {

	@Bean
	public DataSource getDataSource(){
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://192.168.238.55:5432/wcsdbdev");
		dataSource.setUsername("wcsuser");
		dataSource.setPassword("wcsuser");
		return dataSource;
	}

	@Bean
	public LocalSessionFactoryBean getSessionFactory(){
		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
		factoryBean.setDataSource(getDataSource());
		Properties properties = new Properties();
		properties.put("hibernate.show_sql","true");
		properties.put("hibernate.hbm2ddl.auto","update");
		properties.put("hibernate.default_schema", "junior");
		factoryBean.setHibernateProperties(properties);
		factoryBean.setAnnotatedClasses(Book.class, User.class, Cart.class, Role.class);
		return factoryBean;
	}

	@Bean
	public HibernateTransactionManager getTransactionManager(){
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(getSessionFactory().getObject());
		return transactionManager;
	}
}
