package org.example.shop.database.dao;

import org.example.shop.model.Cart;
import org.example.shop.database.service.BookService;
import org.example.shop.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Repository
@Transactional
public class CartDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Cart> findAll() {

        return sessionFactory.getCurrentSession().createQuery("from Cart ").list();
    }

    public Cart findByUser(User user) {

        List<Cart> carts = sessionFactory.getCurrentSession().createQuery("from Cart ").list();
        Cart result = null;

        for (int i = 0; i < carts.size(); i++) {
            if (carts.get(i).getUser().equals(user)) {
                result = findAll().get(i);
                break;
            }
        }

        return result;
    }

    public void save(Cart cart) {

        sessionFactory.getCurrentSession().save(cart);
    }

    public void update(Cart cart) {

        sessionFactory.getCurrentSession().update(cart);
    }

    public void delete(Cart cart) {

        sessionFactory.getCurrentSession().delete(cart);
    }
}
