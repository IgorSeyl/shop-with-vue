package org.example.shop.database.dao;

import org.example.shop.model.Role;
import org.example.shop.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class RoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Role> findAll() {

        return sessionFactory.getCurrentSession().createQuery("from Role ").list();
    }

    public Role findById(int id) {

        return sessionFactory.getCurrentSession().get(Role.class, id);
    }

    public Role findByName(String name) {

        List<Role> roles = sessionFactory.getCurrentSession().createQuery("from Role ").list();
        Role result = null;

        for (Role role : roles) {
            if (role.getName().equals(name)) {
                result = role;
                break;
            }
        }

        return result;
    }

    public void save(Role role) {

        sessionFactory.getCurrentSession().save(role);
    }

    public void update(Role role) {

        sessionFactory.getCurrentSession().update(role);
    }

    public void delete(Role role) {

        sessionFactory.getCurrentSession().delete(role);
    }
}
