package org.example.shop.controller;

import org.example.shop.database.service.UserService;
import org.example.shop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccountController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public ModelAndView account(@ModelAttribute(value = "user") User user) {

        ModelAndView modelAndView = new ModelAndView("account");

        user = userService.findUserById(user.getId());

        modelAndView.addObject(user);

        return modelAndView;
    }
}
