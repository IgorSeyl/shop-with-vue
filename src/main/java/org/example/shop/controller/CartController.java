package org.example.shop.controller;

import org.example.shop.database.service.BookService;
import org.example.shop.database.service.CartService;
import org.example.shop.database.service.UserService;
import org.example.shop.model.Book;
import org.example.shop.model.Cart;
import org.example.shop.model.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
public class CartController {

    @Autowired
    private BookService bookService;
    @Autowired
    private CartService cartService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    public ModelAndView cartAuthorized(@ModelAttribute("user")User user) {

        ModelAndView modelAndView = new ModelAndView("cart");

        Cart cart = cartService.findByUser(user);
        HashMap<Book, Integer> booksToCartJsp = new HashMap<>();

        for (Book book : cart.getBooks()) {

            if (booksToCartJsp.containsKey(book)) booksToCartJsp.put(book, booksToCartJsp.get(book) + 1);
            else booksToCartJsp.put(book, 1);
        }

        modelAndView.addObject("user", user);
        modelAndView.addObject("cart", booksToCartJsp);

        return modelAndView;
    }

    @RequestMapping(value = "/cart", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public void cartAJAX(@RequestBody String jsonMap) throws ParseException {

        Object obj = new JSONParser().parse(jsonMap);
        JSONObject jo = (JSONObject) obj;

        int userId = Integer.parseInt((String) jo.get("userId"));
        boolean isAnon = Boolean.parseBoolean((String) jo.get("isAnon"));
        User user = userService.findUserById(userId);

        String jsonProducts = (String) jo.get("products");

        obj = new JSONParser().parse(jsonProducts);
        jo = (JSONObject) obj;

        List<Book> books = new ArrayList<>();

        for (Iterator<Map.Entry<Integer, Integer>> entries = jo.entrySet().iterator(); entries.hasNext(); ) {
            Map.Entry<Integer, Integer> entry = entries.next();
            int bookId = Integer.parseInt(String.valueOf(entry.getKey()));
            int bookQuantity = Integer.parseInt(String.valueOf(entry.getValue())); // почитать каст типов в json

            Book book = bookService.findBookById(bookId);
            for (int i = 0; i < bookQuantity; i++) {
                books.add(book);
            }
        }

        Cart cart = cartService.findByUser(user);
        if (cart == null) {

            cart = new Cart();
            cart.setUser(user);
            cart.setBooks(books);
            cartService.save(cart);

        } else {
            //cart.setUser(user);
            cart.setBooks(books);
            cartService.update(cart);
        }
    }
}
