package org.example.shop.controller;

import org.example.shop.database.service.BookService;
import org.example.shop.database.service.CartService;
import org.example.shop.database.service.RoleService;
import org.example.shop.database.service.UserService;
import org.example.shop.model.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private BookService bookService;
    @Autowired
    private UserService userService;
    @Autowired
    private CartService cartService;
    @Autowired
    private RoleService roleService;




    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String indexJSONResponse() {

        JSONObject obj = new JSONObject();
        obj.put("name", "mkyong.com");
        obj.put("age", 100);

        JSONArray list = new JSONArray();
        list.add("msg 1");
        list.add("msg 2");
        list.add("msg 3");

        obj.put("messages", list);

        return obj.toString();
    }



//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public ModelAndView index() {
//
//        ModelAndView modelAndView = new ModelAndView("index");
//
////        bookService.saveBook(new Book("Лев Толстой", "Анна Каренина", Style.RUSSIAN_CLASSIC, 1234));
////        bookService.saveBook(new Book("Стивен Кинг", "Сияние", Style.HORROR, 1242));
////        bookService.saveBook(new Book("Джон Толкин", "Властелин колец", Style.FANTASY, 444));
////        bookService.saveBook(new Book("Кто-то", "50 оттенков серого", Style.ROMANTIC, 300));
//
//        User user = new User(roleService.findRoleByName("ANON"));
//
//        userService.saveUser(user);
//
//        modelAndView.addObject("user", user);
//        modelAndView.addObject("books", bookService.findAllBooks());
//
//        return modelAndView;
//    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView indexAuthorized(@ModelAttribute(value = "user") User user) {

        ModelAndView modelAndView = new ModelAndView();

        boolean fromAuthorizationPage =
                user.getId() == 0 && user.getPhoneNumber() == null;
        boolean fromRegistrationPage =
                user.getId() == 0 && user.getPhoneNumber() != null;
        boolean fromOtherPages =
                user.getId() != 0;
        ;
        if (fromOtherPages) {

            user = userService.findUserById(user.getId());

            if (!user.getRole().getName().equals("ANON")) {

                modelAndView.addObject("authorized", true);
            }

            modelAndView.addObject("books", bookService.findAllBooks());
            modelAndView.addObject("user", user);
            modelAndView.setViewName("index");
        }

        if (fromAuthorizationPage) {

            boolean isAdmin = false;
            boolean incorrect = true;

            List<User> users = userService.findAllUsers();
            List<User> notAnonUsers = new ArrayList<>();

            for (User user1 : users) {
                if (!user1.getRole().getName().equals("ANON")) notAnonUsers.add(user1);
            }

            for (int i = 0; i < notAnonUsers.size(); i++) {

                User userToCompare = notAnonUsers.get(i);
                boolean loginMatch = user.getUsername().equals(userToCompare.getUsername());
                boolean passwordMatch = user.getPassword().equals(userToCompare.getPassword());

                if (loginMatch && passwordMatch) {

                    incorrect = false;
                    if (userToCompare.getRole().getName().equals("ADMIN")) isAdmin = true;
                    user = userToCompare;
                    break;
                }
            }

            if (incorrect) {
                modelAndView.addObject("incorrect", true);
                modelAndView.setViewName("authorization");
            }
            else if (isAdmin) modelAndView.setViewName("admin");
            else {
                modelAndView.addObject("books", bookService.findAllBooks());
                modelAndView.addObject("authorized", true);
                modelAndView.addObject("user", user);
                modelAndView.setViewName("index");
            }
        }

        if (fromRegistrationPage) {

            boolean userExists = false;

            List<User> users = userService.findAllUsers();
            List<User> notAnonUsers = new ArrayList<>();

            for (User user1 : users) {
                if (!user1.getRole().getName().equals("ANON")) notAnonUsers.add(user1);
            }

            for (int i = 0; i < notAnonUsers.size(); i++) {

                if (user.getUsername().equals(notAnonUsers.get(i).getUsername())) {

                    userExists = true;
                    break;
                }
            }

            if (userExists) {

                modelAndView.addObject("userExists", true);
                modelAndView.setViewName("registration");
            } else {

                user.setRole(roleService.findRoleByName("REGISTERED"));
                userService.saveUser(user);
                modelAndView.addObject("books", bookService.findAllBooks());
                modelAndView.addObject("authorized", true);
                modelAndView.addObject("user", user);
                modelAndView.setViewName("index");
            }
        }


        return modelAndView;
    }


}
