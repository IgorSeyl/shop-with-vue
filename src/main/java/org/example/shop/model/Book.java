package org.example.shop.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "books_igorseyl")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "author")
    private String author;
    @Column(name = "bookName")
    private String bookName;
    @Column(name = "style")
    @Enumerated(EnumType.STRING)
    private Style style;
    @Column(name = "price")
    private int price;
    @Column(name = "image")
    private String image;

    @ManyToMany
    @JoinTable (name="cart_book_igorseyl",
            joinColumns=@JoinColumn (name="book_id"),
            inverseJoinColumns=@JoinColumn(name="cart_id"))
    private List<Cart> carts;

    @ManyToMany
    @JoinTable (name="user_book_igorseyl",
            joinColumns=@JoinColumn (name="book_id"),
            inverseJoinColumns=@JoinColumn(name="user_id"))
    private List<User> users;

    public Book() {
    }

    public Book(String author, String bookName, Style style, int price) {
        this.author = author;
        this.bookName = bookName;
        this.style = style;
        this.price = price;
        this.image = "resources/img/default-product-pic.png";
    }

    public Book(int id, String author, String bookName, Style style, int price) {
        this.id = id;
        this.author = author;
        this.bookName = bookName;
        this.style = style;
        this.price = price;
        this.image = "resources/img/default-product-pic.png";
    }

    public Book(int id, String author, String bookName, Style style, int price, String image) {
        this.id = id;
        this.author = author;
        this.bookName = bookName;
        this.style = style;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
