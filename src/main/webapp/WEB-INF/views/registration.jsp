<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Регистрация</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/registration.css"/> "/>
</head>
<body>

<div class="form_reg_block">

    <div class="form_reg_block_content">

        <p class="form_reg_block_head_text">Регистрация</p>

        <form:form id="registration_form" class="form_reg_style" modelAttribute="user" method="post" acceptCharset="UTF-8"
                   action="${pageContext.request.contextPath}/">
            <form:input path="username" type="text" placeholder="Вашe имя" required="required"/>
            <form:input path="password" type="password" placeholder="Ваш пароль" required="required"/>
            <form:input path="phoneNumber" type="text" placeholder="Ваш телефон" required="required" minlength="17" maxlength="17" id="tel"/>
            <form:input path="address" type="text" placeholder="Ваш адрес" required="required"/>
        </form:form>

        <div class="buttons">
            <button class="form_reg_button" type="submit" form="registration_form">Зарегистрироваться</button>
            <form class="form_exit" action="${pageContext.request.contextPath}/">
                <button type="submit" class="exit">Выход</button>
            </form>
        </div>

<%--        <p>${usernameError}</p>--%>
        <c:if test="${userExists}">
            <p>Под этим именем уже есть другой пользователь. Попробуйте другой!</p>
        </c:if>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/phone-number.js"></script>

</body>
</html>
